<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property=fb:app_id content=1729947243944912>
		<meta property=og:site_name content={{$name}}>
		<meta name=description content="Scopri che pokémon sei scrivendo pok.fan/tuonome">
		<meta property=og:title content={{$name}}>
		<meta property=og:description content="Scopri in che città vivrai tra 10 anni con cit.fun/tuonome">
		<meta property=og:type content=website>
		<meta name=twitter:title content={{$name}}>
		<meta name=twitter:card content=photo>
		<meta property=og:url content=http://www.letmeapp.com/it/city/karmin>
		<meta name=twitter:image content=http://2.bp.blogspot.com/_E1pKqN_vRHo/TJek_j6eYqI/AAAAAAAAAGY/C7kqz7KZKZA/s1600/ColLibertad.jpg>
		<meta property=og:image content=http://2.bp.blogspot.com/_E1pKqN_vRHo/TJek_j6eYqI/AAAAAAAAAGY/C7kqz7KZKZA/s1600/ColLibertad.jpg>
		<meta property=og:image:type content=jpeg>
		<meta property=og:image:width content=1500>
		<meta property=og:image:height content=844>

        <title>Risultato</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
		<link href="/assets/css/app.css" rel="stylesheet" type="text/css">
	</head>
    <body>
	<div class="wrapper">
		<div class="container">

			<h2 class="text-center">Il pokémon che ti rappresenta è:</h2>
			<h1 class="text-center">{{$name}}</h1>
			<img src="{{$image}}" alt="" class="img-responsive">
			<p class="text-center">{{$description}}</p>
		</div>
	</div>
    </body>
</html>
