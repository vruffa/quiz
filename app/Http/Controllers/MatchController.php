<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Object;
use App\ObjectName;

class MatchController extends Controller
{
    //
    //
    public function find($name)
    {
        if($object_name = ObjectName::where('name', $name)->first())
        {
            $object = Object::find($object_name->object_id);
        }
        else
        {
            $object = Object::inRandomOrder()->first();
            ObjectName::create(['name' => $name, 'object_id' => $object->id]);
        }

		return view('result',
				[
						'name' => $object->name,
						'description' => '',
						'image' => ''
				]
		);
    }
}
