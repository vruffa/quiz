<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObjectName extends Model
{
    protected $table = "objects_names";
    protected $fillable = ['name', 'object_id'];
}
